import { useHistory } from "react-router";
import { Container } from "./styles";

const Dashboard = () => {
  const history = useHistory();
  return (
    <Container>
      <h1>
        Obrigado pela compra volte sempre!!
        <button
          onClick={() => {
            localStorage.clear();
            history.push("/");
            window.location.reload();
          }}
        >
          Voltar as Compras
        </button>
      </h1>
    </Container>
  );
};

export default Dashboard;

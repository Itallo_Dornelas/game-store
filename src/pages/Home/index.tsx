import { Container, ProductList, Title } from "./styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useCarts } from "../../providers/cart";
import { useEffect, useState } from "react";
import api from "../../services/api";
import formatValue from "../../utils";

function Home() {
  const { setCart, cart } = useCarts();
  interface Product {
    id?: number;
    title: string;
    description: string;
    price: number;
    image: string;
  }
  const [products, setProducts] = useState<Product[]>([] as Product[]);
  const [loading, setLoading] = useState(true);

  async function loadProducts() {
    const response = await api.get("/products/");

    const data = response.data.map((product: Product) => ({
      ...product,
      priceFormatted: formatValue(product.price),
    }));

    setLoading(false);
    setProducts(data);
  }

  useEffect(() => {
    loadProducts();
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <Title>Os melhores celulares e os melhores preços!!</Title>
      <Container>
        {loading ? (
          <CircularProgress size={50} />
        ) : (
          <ProductList>
            {products.map((product) => (
              <li key={product.id}>
                <figure>
                  <img src={product.image} alt={product.title} />
                </figure>
                <h2>{product.title}</h2>
                <div>
                  <span>{formatValue(product.price)}</span>
                  <button onClick={() => setCart([...cart, product])}>
                    <span>Adicionar ao Carrinho</span>
                  </button>
                </div>
              </li>
            ))}
          </ProductList>
        )}
      </Container>
    </div>
  );
}
export default Home;

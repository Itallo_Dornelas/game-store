import { Link } from "react-router-dom";
import styled from "styled-components";
export const ContainerHeader = styled.div`
  width: 100%;
  margin: 0 auto;
  overflow: hidden;
  position: relative;
  background-color: var(--blue);
  background-image: linear-gradient(62deg, var(--blue) 0%, #224959 100%);
  box-shadow: 0.2rem 0.3rem 2px rgba(0, 0, 0, 0.6);

  div {
    display: flex;
    justify-content: space-between;
    text-align: center;
    align-items: center;
  }
`;
export const NavLink = styled(Link)`
  margin: 10px;
  font-size: 18px;
  color: var(--blue);
  :hover {
    color: var(--white);
  }
  h3 {
    color: var(--gray);
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    font-size: 2.5rem;
    span {
      color: #c85311;
    }
  }
  h3:hover {
    color: var(--white);
  }
  > svg {
    transform: translateY(3.5px);
    margin-right: 10px;
  }
  span {
    margin-left: 5px;
  }
`;

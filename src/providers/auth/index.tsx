import { SetStateAction, Dispatch } from "react";
import { createContext, ReactNode, useContext, useState } from "react";

import { toast } from "react-toastify";
import api from "../../services/api";

interface Auth {
  id?: number;
  username: string;
  password: string;
}
interface History {
  push: any;
}

interface AuthProviderData {
  auth: Auth[];

  setAuth: Dispatch<SetStateAction<Auth[]>>;
  signIn: (data: Auth, history: History) => void;
}

interface AuthProviderProps {
  children: ReactNode;
}

const AuthContext = createContext<AuthProviderData>({} as AuthProviderData);

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const [auth, setAuth] = useState<Auth[]>([] as Auth[]);

  const signIn = (data: Auth, history: History) => {
    api
      .post("sessions/", data)
      .then((response) => {
        localStorage.setItem("@KenzieShop:token", response.data.access);
        setAuth(response.data.access);
        history.push("/dashboard");
        toast.success("Você foi Logado");
      })
      .catch((_) =>
        toast.error("Falha na autenticação, verifique seus dados!!")
      );
  };

  return (
    <AuthContext.Provider value={{ auth, signIn, setAuth }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);

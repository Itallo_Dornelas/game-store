import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  position: relative;
  @media (min-width: 571px) {
    flex-direction: row;
  }
`;

export const ContainerRegister = styled.div`
  width: 100%;
  height: 350px;
  margin-top: 10px;
`;

export const ContainerForm = styled.div`
  display: flex;
  width: 90%;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  padding: 10px;
  background: var(--vanilla);
  border: 1px solid transparent;
  background: linear-gradient(var(--blue) 0%, var(--vanilla) 100%);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25),
    inset 0px 4px 4px rgba(0, 0, 0, 0.25);
  @media (min-width: 520px) {
    width: 75%;
  }
  @media (min-width: 1100px) {
    width: 40%;
    height: 500px;
  }

  h2 {
    font-size: 18px;
    font-weight: 400;
    margin: 20px;
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  }
  button {
    margin: 5px auto;
    background: var(--blue);
    color: var(--vanilla);
    border: 0;
    border-radius: 4px;
    transition: 200ms ease-in-out;
    padding: 5px;
    text-align: center;
    font-weight: bold;

    :hover {
      background-color: #224959;
    }
  }

  h4 {
    margin: 10px;
    a {
      color: var(--orange);
      font-weight: bold;
    }
  }
`;

import styled from "styled-components";

import { Link } from "react-router-dom";
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
`;
export const NavLink = styled(Link)`
  margin: 10px auto;
  width: 120px;
  font-size: 18px;
`;
export const ContainerLogin = styled.div`
  margin-top: 10px;
  width: 100%;
  height: 350px;
`;
export const ContainerForm = styled.div`
  display: flex;
  width: 80%;
  flex-direction: column;
  align-items: center;
  color: var(--white);
  margin: 0 auto;
  background: var(--blue);
  border: 1px solid transparent;
  background: linear-gradient(var(--blue) 0%, var(--gray) 100%);
  border-image-slice: 1;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25),
    inset 0px 4px 4px rgba(0, 0, 0, 0.25);
  @media (min-width: 520px) {
    width: 75%;
  }
  @media (min-width: 1100px) {
    width: 40%;
    height: 450px;
  }

  h2 {
    font-size: 18px;
    font-weight: 400;
    margin: 20px;
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  }
  button {
    margin: 5px auto;
    background: var(--blue);
    color: var(--vanilla);
    border: 0;
    border-radius: 4px;
    transition: 200ms ease-in-out;
    padding: 5px;
    text-align: center;
    font-weight: bold;

    :hover {
      background-color: #224959;
    }
  }

  h4 {
    margin: 10px;
    a {
      color: var(--orange);
      font-weight: bold;
    }
  }
`;

import { RiShoppingCartLine } from "react-icons/ri";
import { ContainerHeader, NavLink } from "./styles";
import { useCarts } from "../../providers/cart";
import { Badge } from "@material-ui/core";

export default function PrimarySearchAppBar() {
  const { cart } = useCarts();
  return (
    <ContainerHeader>
      <div>
        <NavLink to="/">
          <h3>
            Kenzie Shop.<span>Celulares</span>
          </h3>
        </NavLink>
        <nav>
          <NavLink to="/cart">
            <Badge badgeContent={cart.length} color="primary">
              <RiShoppingCartLine size={20} />
            </Badge>
            <span> Carrinho </span>
          </NavLink>
        </nav>
      </div>
    </ContainerHeader>
  );
}

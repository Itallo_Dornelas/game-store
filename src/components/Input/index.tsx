import { FieldValues, UseFormRegister } from "react-hook-form";
import { IconType } from "react-icons";
import { Container, InputContainer } from "./styles";
interface InputProps {
  error: string;
  label: string;
  register: UseFormRegister<FieldValues>;
  name: string;
  type?: string;
  icon: IconType;
  placeholder: string;
}
function Input({
  placeholder,
  type,
  error = "",
  label,
  register,
  icon: Icon,
  name,
  ...rest
}: InputProps) {
  return (
    <Container>
      <div>
        {label}
        {!!error && <span> - {error}</span>}
      </div>
      <InputContainer isErrored={!!error}>
        {Icon && <Icon />}
        <input
          placeholder={placeholder}
          type={type}
          {...register(name)}
          {...rest}
        />
      </InputContainer>
    </Container>
  );
}
export default Input;
